"""energy_analysis module"""
import os
import datetime
import warnings
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import requests
from pmdarima import auto_arima
from statsmodels.tsa.arima.model import ARIMA


class EnergyAnalysis:
    """
    Analysis tool to analyse the energy consumption of various countries.

    Attributes
    ----------
    dataframe: object
        Pandas dataframe of the downloaded energy data

    Methods
    --------
    countries()
        Lists all available iso-codes

    consumption()
        Plots a stackchart with year on the x-axis and energy consumption on y-axis

    total_consumption_emission()
        Plots "years" against "total consumption" and "total emission"

    gdp_evolution()
        Plots a chart with "years" against "gdp"

    gapminder()
        Plots a scatter plot with GDP (x-axis, log) and total energy consumption (y-axis, log)

    forecast()
        Plots 2 line charts predicting "tota_consumption" and "total_emission"
    """

    def __init__(self):
        # Set up
        filename = "data"
        folder = "downloads/"
        pathname = os.path.join(folder, filename + ".csv")

        if os.path.exists(folder):
            if os.path.isfile(pathname) is False:
                url = "https://nyc3.digitaloceanspaces.com/owid-public/data/energy/owid-energy-data.csv"
                res = requests.get(url, allow_redirects=True)
                file = open(pathname, "wb")
                file.write(res.content)
                file.close()
                print("file created in downloads folder")

            else:
                print("file already exists")
        else:
            os.mkdir(folder)
            url = "https://nyc3.digitaloceanspaces.com/owid-public/data/energy/owid-energy-data.csv"
            res = self.requests.get(url, allow_redirects=True)
            file = open(pathname, "wb")
            file.write(res.content)
            file.close()
            print("Downloads folder created and data saved to new downloads folder")

        initial_df = pd.read_csv(pathname)

        # Add total emissions column
        factor = 10 ** (
            9 - 6
        )  # 1Twh = 10^9 Kwh, 1t = 1/10^6 g, from g/Twh --> t/Kwh --> *10^9/10^6
        initial_df["total_emission"] = factor * (
            initial_df["biofuel_consumption"] * 1450
            + initial_df["coal_consumption"] * 1000
            + initial_df["gas_consumption"] * 455
            + initial_df["hydro_consumption"] * 90
            + initial_df["nuclear_consumption"] * 5.5
            + initial_df["oil_consumption"] * 1200
            + initial_df["solar_consumption"] * 53
            + initial_df["wind_consumption"] * 14
        )

        # Add total_consumption column
        consumption_cols = [col for col in initial_df.columns if "_consumption" in col]
        dropcols = [
            "fossil_fuel_consumption",
            "primary_energy_consumption",
            "renewables_consumption",
        ]
        for column in dropcols:
            consumption_cols.remove(column)
        initial_df["total_consumption"] = initial_df[consumption_cols].sum(axis=1)

        # Datetime & index adjustments

        self.dataframe = initial_df.loc[initial_df["year"] >= 1970]

        pd.options.mode.chained_assignment = None  # default='warn'
        self.dataframe["year"] = pd.to_datetime(self.dataframe["year"], format="%Y")
        self.dataframe = self.dataframe.set_index(["year", "country"])
        self.dataframe = self.dataframe.sort_index()

        # Define global variables

        global countries
        countries = []

    def countries(self) -> list:
        """
        Function that lists all unique iso-codes available within data set.

        Parameters
        -----------
        self: class
            The energy analysis class itself

        Returns
        ---------------
        countries: list
            List of all available iso-codes.
        """

        return list(self.dataframe["iso_code"].unique())

    def consumption(self, country: str, normalize: bool):
        """
        Function first checks if the entered iso-code (country) is valid.
        If not a ValueError is raised.
        The method plots an area chart of the energy consumption for the selected country.
        Hereby, the total / relative energy consumption is dispayed for each year.
        Each type of consumption is dispayed in a different color.
        The charts shows all energy consumption types absolute and relative (to each other).

        Parameters
        ---------------
        self: class
            The energy analysis class itself

        country: str
            Iso-code of the country the user chooses to analyze.

        normalize: bool
            Boolean that determines whether energy consumption is normalized.

        Returns
        ---------------
        energy consumption: stackplot
            A stackplot with year on the x-axis and energy consumption on y-axis.

        """

        if country not in list(self.dataframe["iso_code"]):
            print("raising value error")
            raise ValueError(
                f'Country with iso-code "{country}" does not exist. \
                \nPlease enter a valied iso-code. \
                \nTo check available iso-codes, use the countries() method.'
            )

        df_consumption = self.dataframe.copy()
        df_consumption = df_consumption.loc[df_consumption["iso_code"] == country]

        consumption_cols = [col for col in df_consumption if "_consumption" in col]

        dropcols = [
            "fossil_fuel_consumption",
            "primary_energy_consumption",
            "renewables_consumption",
            "total_consumption",
        ]

        for column in dropcols:
            consumption_cols.remove(column)

        for col in df_consumption[consumption_cols].columns:
            if normalize is False:
                df_consumption[col] = df_consumption[col]
            else:
                df_consumption = df_consumption[consumption_cols]
                df_consumption = df_consumption.div(df_consumption.sum(axis=1), axis=0)
                df_consumption = df_consumption.reset_index()

        get_years = np.asarray(df_consumption.index.get_level_values(0))
        get_consumption = np.asarray(df_consumption[consumption_cols])
        colors = plt.cm.tab20c((4.0 / 3 * np.arange(20 * 3 / 4)).astype(int))
        plt.stackplot(get_years, get_consumption.T, labels=consumption_cols, colors=colors)
        plt.legend(loc="center left", bbox_to_anchor=(1, 0.5))
        plt.title(country)
        plt.show()

    def countries_analysis(self):
        """
        Function that asks the user for input regarding the iso-codes to be analysed.
        The returned 'countries' list can be used as a parameter in other functions.

        Parameters
        -----------
        self: class
            The energy analysis class itself

        Returns
        ---------------
        countries: list
            List of iso-codes of the countries to be analysed.

        """

        countries = []
        stop = False
        while stop is not True:
            country = input(
                "Please enter the iso_code of the country that you would like to add. \
                \nWhen you are done, type 'exit': "
            ).upper()
            if country == "EXIT":
                stop = True
                print(" You selected: ", countries)
            elif country in countries:
                print(country + " is already selected")
            elif country not in list(self.dataframe["iso_code"].unique()):
                print(
                    f'Country with iso-code "{country}" does not exist. \
                    \nPlease enter a valied iso-code. \
                    \nTo check available iso-codes, use the countries() method.'
                )
            else:
                countries.append(country)
                print(country + " has been added successfully")
        return countries

    def total_consumption_emission(self, countries: list):
        """
        A function that displays the total energy consumption for every country selected.
        "iso_code", "year", and "total_consumption" will be combined in a data frame.
        The data frame will filter for out all data after 2020.
        A line chart plots "years" against "total_consumption" for every country selected.

        Parameters
        ---------------
        self: class
            The energy analysis class itself

        countries: list
            A list with all countries the user chooses to analyze

        Returns
        ---------------
        total: chart
            A chart plots "years" against "total" consumption for each country asked
        """

        relevant_cols = ["iso_code"] + ["total_consumption"] + ["total_emission"]
        plot_df = self.dataframe.loc[
            self.dataframe["iso_code"].isin(countries), relevant_cols
        ]
        plot_df = plot_df.iloc[
            plot_df.index.get_level_values("year") < datetime.datetime(2020, 1, 1)
        ]

        fig, ax1 = plt.subplots(figsize=(20, 8))
        ax2 = ax1.twinx()
        ax1.set_ylabel("Total consumption in Twh (full lines)")
        ax2.set_ylabel("Total emissions in tonnes of CO2 (dashed lines)")
        plt.xlabel("Years")
        color_count = 0

        for group in plot_df["iso_code"].unique():
            chart_line = plot_df[plot_df["iso_code"] == group]
            colors = ["red", "blue", "green", "orange", "purple", "brown", "olive"]
            ax1.plot(
                "year",
                "total_consumption",
                data=chart_line.reset_index(),
                color=colors[color_count],
                label=group,
            )
            ax2.plot(
                "year",
                "total_emission",
                data=chart_line.reset_index(),
                color=colors[color_count],
                linestyle="dashed",
                label=group,
            )
            color_count += 1
            ax1.legend(loc="center left", bbox_to_anchor=(1, 1))

    def gdp_evolution(self, countries: list):
        """
        For every country, the parameters: "iso_code", "year", "gdp" will combined in a data frame.
        A line chart plots "years" against "gdp".
        Every selected country represents by one chart line.

        Parameters
        ---------------
        self: class
            The energy analysis class itself

        countries_selected: list
            A list with all countries the user chooses to analyze

        Returns
        ---------------
        gdp: chart
            A line chart plotting "years" against "gdp" for each country asked
        """

        relevant_cols = ["iso_code", "gdp"]
        plot_df = self.dataframe.loc[
            self.dataframe["iso_code"].isin(countries), relevant_cols
        ]
        fig, ax = plt.subplots(figsize=(20, 8))
        plt.xlabel("Years")
        plt.ylabel("GDP")

        for group in plot_df["iso_code"].unique():
            chart_line = plot_df[plot_df["iso_code"] == group]
            ax.plot("year", "gdp", data=chart_line.reset_index(), label=group)
            ax.legend()

    def population_evolution(self, countries: list):
        """
        For every country, the following parameters will combined in a data frame:
        "iso_code", "year", "population".
        A line chart plots "years" against "population".

        Parameters
        ---------------
        self: class
            The energy analysis class itself

        countries_selected: list
            A list with all countries the user chooses to analyze

        Returns
        ---------------
        gdp: chart
            A line chart plotting "years" against "population" for each country asked.
        """

        relevant_cols = ["iso_code", "population"]
        plot_df = self.dataframe.loc[
            self.dataframe["iso_code"].isin(countries), relevant_cols
        ]
        fig, ax = plt.subplots(figsize=(20, 8))
        plt.xlabel("Years")
        plt.ylabel("Population")

        for group in plot_df["iso_code"].unique():
            chart_line = plot_df[plot_df["iso_code"] == group]
            ax.plot("year", "population", data=chart_line.reset_index(), label=group)
            ax.legend(loc="center left")

    def gapminder(self, year: int):
        """
        For all countries the following parameters, will be combined in a data frame:
        "iso_code", "year", "gdp", "total_consumption", "population".
        A scatter plot will plot "gdp" against "total_consumption".
        The size "population" will define the size of the scatter plots.

        Parameters
        ---------------
        self: class
            The energy analysis class itself

        year: int
            Year that determines which year will be compared

        Returns
        ---------------
        total: chart
            Scatter plot: GDP (x-axis, log), total consumption (y-axis, log), population (dot size).
        """

        try:
            if isinstance(year, int) != True:
                raise TypeError

        except:
            print("The specified year is not of type integer")

        else:
            relevant_cols = ["iso_code", "gdp", "population", "total_consumption"]
            plot_df = self.dataframe.loc[[datetime.datetime(year, 1, 1)]][relevant_cols]
            plot_df.dropna(inplace=True)

            list_iso_code = plot_df["iso_code"].tolist()
            list_gdp = plot_df["gdp"].tolist()
            list_total_consumption = plot_df["total_consumption"].tolist()
            size = plot_df["population"].to_numpy() * 10 ** (-7)

            fig, ax = plt.subplots(figsize=(20, 8))
            scatter = ax.scatter(
                x=plot_df["gdp"], y=plot_df["total_consumption"], s=size
            )

            plt.title(year)
            plt.xlabel("GDP (log.)")
            plt.ylabel("Total energy consumption (log.)")
            ax.set_xscale("log")
            ax.set_yscale("log")

            handles, labels = scatter.legend_elements(prop="sizes", alpha=0.6, num=5)
            ax.legend(
                handles, labels, loc=(1.04, 0), title="Population in tens of million", \
                prop={'size': 16}
            )

            for i, txt in enumerate(list_iso_code):
                ax.annotate(txt, (list_gdp[i], list_total_consumption[i]))

            plt.show()

    def forecast(self):
        """
        A function to forecast energy consumption and energy emission in 2 line charts.
        The function makes the user define two inputs:
        a country, a number of periods to forecast.
        It will raise an exception in case:
        - The users enters a non-existent iso-code
        - The user does not enter an integer for the predicted periods
        - The user enters an integer that is 1 or below 1
        The function creates a dataframe to select all parameters for the computation:
        "year" (in the index), "total_emission", and "total_consumption".
        After doing so, the Auto-ARIMA function will run for both, energy consumption and emission.
        The result will be a model from the ARIMA family, tailored to the chosen country.
        The respective models for energy consumption and emission will be applied.
        The reult is visualized by 2 side-by-side line charts.

        Parameters
        ---------------
        self: class
            The energy analysis class itself

        Returns
        ---------------
        total: chart
            Two side-by-side line charts.
            The left line chart plot "years" against "total_consumption".
            The right line chart will plot "years" against"total_emission".

        """

        # Input Section
        stop_1 = False
        stop_2 = False
        while stop_1 is False:
            try:
                pred_country = input(
                    "Please enter a country's iso code to predict total consumption & emission: "
                ).upper()
                if pred_country not in self.dataframe["iso_code"].unique():
                    raise ValueError

            except ValueError:
                print(
                    f'Country with iso-code "{pred_country}" does not exist. \
                    \nPlease enter a valied iso-code. \
                    \nTo check available iso-codes, use the countries() method.'
                )

            else:
                stop_1 = True
                while stop_2 is False:
                    try:
                        pred_period = int(
                            input(
                                "Please enter how many periods you would like to forecast. \
                                \nPlease enter an integer above '1': "
                            )
                        )
                        if pred_period <= 1:
                            raise

                    except ValueError:
                        print(
                            "The specified forecast period is not of type integer. \
                            \nPlease enter an integer."
                        )
                    except:
                        print(
                            "The specified forecast period has to be above 1 ('2', '3', ...). \
                            \nPlease enter a valid forecast period."
                        )

                    else:
                        stop_2 = True

        # New Dataframe
        forecast_df = self.dataframe.loc[self.dataframe["iso_code"] == pred_country]
        forecast_df.reset_index(inplace=True)
        forecast_df = (
            forecast_df[["year", "total_consumption", "total_emission"]]
            .replace({0: np.NaN})
            .dropna()
        )
        forecast_df.set_index("year", inplace=True)

        # Auto Arima Consumption
        warnings.filterwarnings("ignore")

        try:  # Only ARIMA parameter were chosen, not SARIMAX, as there is no seasonality
            stepwise_fit = auto_arima(
                forecast_df["total_consumption"],
                start_p=1,
                start_q=1,
                max_p=3,
                max_q=3,
                d=None,
                trace=True,
                error_action="ignore",
                suppress_warnings=True,
                stepwise=True,
            )
            output_order_consumption = stepwise_fit.order

        except:
            output_order_consumption = (2, 1, 1)

        # Auto Arima Emission
        try:
            stepwise_fit = auto_arima(
                forecast_df["total_emission"],
                start_p=1,
                start_q=1,
                max_p=3,
                max_q=3,
                d=None,
                trace=True,
                error_action="ignore",
                suppress_warnings=True,
                stepwise=True,
            )
            output_order_emission = stepwise_fit.order

        except:
            output_order_emission = (2, 1, 1)

        # ARIMA Forecast total_consumption
        consumption_model = ARIMA(
            forecast_df["total_consumption"], order=output_order_consumption
        )
        consumption_model_fit = consumption_model.fit()
        #consumption_predictions_past = consumption_model_fit.predict(1, len(forecast_df)-1)
        consumption_predictions_future = consumption_model_fit.predict(
            len(forecast_df), len(forecast_df) + pred_period - 1
        )

        emission_model = ARIMA(
            forecast_df["total_emission"], order=output_order_emission
        )
        emission_model_fit = emission_model.fit()
        #emission_predictions_past = emission_model_fit.predict(1, len(forecast_df) + pred_period-1)
        emission_predictions_future = emission_model_fit.predict(
            len(forecast_df), len(forecast_df) + pred_period - 1
        )

        # Plot
        fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(20, 8))
        fig.suptitle(
            f"Prediction for total energy consumption & total emission of country: \
            \n'{pred_country}' for the next '{pred_period}' years."
        )

        ax1.plot(
            forecast_df["total_consumption"],
            label="Historical Consumption",
            color="blue",
        )
        # ax1.plot(consumption_predictions_past, label = "Past Model Prediction", \
        # color = "green", linestyle='dashed')
        ax1.plot(
            consumption_predictions_future,
            label="Future Model Prediction",
            color="red",
            linestyle="dashed",
        )
        ax2.plot(
            forecast_df["total_emission"], label="Historical Emission", color="blue"
        )
        # ax2.plot(emission_predictions_past, label = "Past Model Prediction", \
        # color = "green", linestyle='dashed')
        ax2.plot(
            emission_predictions_future,
            label="Future Model Prediction",
            color="red",
            linestyle="dashed",
        )

        ax1.set_ylabel("Energy Consumption in TWh")
        ax2.set_ylabel("CO2 Emission in Tonnes")
        ax1.set_xlabel("Years")
        ax2.set_xlabel("Years")

        ax1.legend(loc="upper left")
        ax2.legend(loc="upper left")

    def energycomparison(self):
        """
        For all countries, the following parameters will combined in a data frame:
        "iso_code", "year", "total_emission", "total_consumption", "population".
        The user choses which year he would like to plot.
        The user choses how many years backwards he would like to plot.
        The user gets informed whether inputs are valid with regards to 'type' or 'value'.
        A scatter plot will plot the results.

        Parameters
        ---------------
        self: class
            The energy analysis class itself

        Returns
        ---------------
        total: chart
            Scatter plot: total consumption
            (x-axis, log), total emission (y-axis, log), population (dot size).

        """

        # Input
        stop_1 = False
        stop_2 = False

        while stop_1 is False:
            try:
                end_year = int(
                    input(
                        "Please enter the last year you would like to plot. \
                        \nWe advise the year '2016': "
                    )
                )
                if end_year not in range(1970, 2020):
                    raise ValueError
                if isinstance(end_year, int) != True:
                    raise TypeError
            except ValueError:
                print("Year must be between '1970' and '2019'")
            except TypeError:
                print("Year is not of type integer")
            else:
                stop_1 = True
                while stop_2 is False:
                    try:
                        period = int(
                            input(
                                "Please enter the number of past years you would like to plot. \
                                \nWe advise '5' years: "
                            )
                        )
                        if end_year - period < 1970 or period < 1:
                            raise ValueError
                    except ValueError:
                        maximum = end_year - 1970
                        print("'Number of past years' has to be between 1 and", maximum)
                    except TypeError:
                        print("'Number of past years' is not of type integer")
                    else:
                        stop_2 = True

        # Plot
        for year in range(end_year - period, end_year + 1):
            relevant_cols = [
                "iso_code",
                "population",
                "total_consumption",
                "total_emission",
            ]
            plot_df = self.dataframe.loc[[datetime.datetime(year, 1, 1)]][relevant_cols]
            plot_df.dropna(inplace=True)

            list_iso_code = plot_df["iso_code"].tolist()
            list_total_consumption = plot_df["total_consumption"].tolist()
            list_total_emission = plot_df["total_emission"].tolist()
            size = plot_df["population"].to_numpy() * 10 ** (-7)
            
            fig, ax = plt.subplots(figsize=(20, 8))
            scatter = ax.scatter(
                x=plot_df["total_consumption"], y=plot_df["total_emission"], s=size
            )

            plt.title(year)
            plt.xlabel("Total energy consumption (log.)")
            plt.ylabel("Total energy emission (log.)")
            ax.set_xscale("log")
            ax.set_yscale("log")

            handles, labels = scatter.legend_elements(prop="sizes", alpha=0.6, num=5)
            ax.legend(
                handles, labels, loc=(1.04, 0), title="Population in tens of million", \
                prop={'size': 16}
            )

            for i, txt in enumerate(list_iso_code):
                ax.annotate(txt, (list_total_consumption[i], list_total_emission[i]))

            plt.show()
