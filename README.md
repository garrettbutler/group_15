# group_15

AdPro Group Project T3 - 2022

## Adpro Project Group 15

## Description
This is the Advanced Programming Group Assignment solution by Garrett Butler, Felix Kocher, and Maarten De Winter

## Installation

Startpoint: 
Clone this repository on your local device. We assume by now you have installed a type of conda. The following steps will allow the user to perform the analysis provided in Showcase.ipynb:

1. Open Anaconda prompt ("terminal" if you work on MAC)
2. In your prompt, locate the downloaded repository folder.
3. Enter the following line of code:

- conda env create --name AP15 --file AP15.yaml

4. Run the environment and open jupyterlab

Either via Anaconda Navigator or via prompt, code: 
- conda activate AP15
- jupyter-lab (jupyter lab on MAC)

## Support
Maarten De Winter - 48931@novasbe.pt

Felix Kocher - 50308@novasbe.pt

Garrett Butler - 48763@novasbe.pt

## Roadmap
Day One of Two (completed)

Day Two of Two (completed)

## Authors and acknowledgment
Maarten De Winter - 48931@novasbe.pt

Felix Kocher - 50308@novasbe.pt

Garrett Butler - 48763@novasbe.pt

## License
GNU GENERAL PUBLIC LICENSE

## Project status
Finished
